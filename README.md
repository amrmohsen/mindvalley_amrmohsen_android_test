# README #

### What is this repository for? ###

* Quick summary about Universal Loader Lib: this is a network lib that is being used to load Json and Images asynchronously by creating a thread for each request, it also cache the results in memory (not disk) efficiently and can remove the not used images when there is a need for a space. The Lib was built in a way to be extended to include other datatype and that can be done by inheriting two classes from Request.java & Loader.java, that will ensure that the lib will be able to handle any datatype in the future.
It can run parallel requests smoothly but each request always has to have a UNIQUE ID so make sure to always assign a unique to your request
* Version: 1.1.5

### How do I get set up? ###

* Summary of set up
* Configuration: All you need to do is to clone the project or if you just need to import the Lib only, you can clone it from [here](https://bitbucket.org/amrmohsen/universal-loader/overview). will convert the lib to maven soon for better and easier integration
* How to run tests? I didn't have time to build the unit test cases but will do that ASAP.

### Who do I talk to? ###

* Repo owner: bitbucket/amrmohsen
* Email: amrmohsen91@gmail.com