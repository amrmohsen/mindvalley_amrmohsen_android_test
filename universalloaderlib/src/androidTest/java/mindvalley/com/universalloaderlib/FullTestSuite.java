package mindvalley.com.universalloaderlib;

import android.test.suitebuilder.TestSuiteBuilder;

import junit.framework.Test;
import junit.framework.TestSuite;

public class FullTestSuite extends TestSuite {

    public static Test suit() {
        return new TestSuiteBuilder(FullTestSuite.class).includeAllPackagesUnderHere().build();

    }

    public FullTestSuite() {
        super();
    }


}
