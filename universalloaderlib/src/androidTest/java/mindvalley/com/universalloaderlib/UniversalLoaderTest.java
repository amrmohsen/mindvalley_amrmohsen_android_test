package mindvalley.com.universalloaderlib;

import android.app.Activity;
import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.filters.MediumTest;
import android.support.test.runner.AndroidJUnit4;
import android.test.AndroidTestCase;
import android.util.Log;

import org.junit.Test;
import org.junit.runner.RunWith;

import mindvalley.com.universalloaderlib.listeners.ErrorResponseListener;
import mindvalley.com.universalloaderlib.listeners.ResponseListener;
import mindvalley.com.universalloaderlib.request.datatype.JsonRequest;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@MediumTest
@RunWith(AndroidJUnit4.class)
public class UniversalLoaderTest extends AndroidTestCase {

    String jsonResult;

    @Test
    public void useAppContext() throws Exception {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        Log.d("text",appContext.getPackageName());
        System.out.println(appContext.getPackageName());
        assertEquals("com.mindvalley.universalloader", appContext.getPackageName());
    }

    @Test
    public void test1_executeJsonRequest() {

        JsonRequest jsonRequest=new JsonRequest((Activity) getContext());
        jsonRequest.setId(2);
        jsonRequest.setResponseListener(new ResponseListener() {
            @Override
            public void onSuccess(Object o) {
                jsonResult=o.toString();
            }
        });
        jsonRequest.setErrorResponseListener(new ErrorResponseListener() {
            @Override
            public void onError(Exception e) {
            }
        });
        jsonRequest.load("http://pastebin.com/raw/wgkJgazE");

        assertTrue(jsonResult  != null );
    }
}
