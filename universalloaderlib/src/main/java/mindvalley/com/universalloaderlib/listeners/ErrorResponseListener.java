package mindvalley.com.universalloaderlib.listeners;

/**
 * Interface to be used as a listener when the request occurs an error
 */

public interface ErrorResponseListener {


    /**
     * this method handles the exception that may occur with the request
     * @param e the exception occured while the request was being executed
     */
    public void onError(Exception e);

}
