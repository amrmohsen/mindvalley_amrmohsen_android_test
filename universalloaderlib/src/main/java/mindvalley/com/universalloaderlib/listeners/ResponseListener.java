package mindvalley.com.universalloaderlib.listeners;

/**
 * Interface to be used as a Response listener when the request is successfully completed
 */

public interface ResponseListener {

    /**
     * this method deliver the data when the request is successfully executed
     * @param o the result data of the request
     */
    public void onSuccess(Object o);
}
