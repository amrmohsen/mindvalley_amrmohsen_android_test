package mindvalley.com.universalloaderlib.request;

import android.app.Activity;

import mindvalley.com.universalloaderlib.listeners.ErrorResponseListener;
import mindvalley.com.universalloaderlib.listeners.ResponseListener;

/**
 * Abstract Request class that can be used as a parent class to newly created datatype requests
 * such as XmlRequest, this allow the lib to be scaled to easily extend new types of requests
 */

public abstract class Request {

    private int id;
    private Activity context;

    private ResponseListener responseListener;
    private ErrorResponseListener errorResponseListener;

    public Request(Activity context) {
        this.context = context;

    }

    /**
     *
     * @param id the unique id to create the request
     * @param context the activity context
     * @param responseListener new response listener to get the data when the request is successfully finished
     * @param errorResponseListener new error response listener when the request is failed to be executed
     */
    public Request(int id, Activity context, ResponseListener responseListener, ErrorResponseListener errorResponseListener) {
        this.id=id;
        this.context = context;
        this.responseListener = responseListener;
        this.errorResponseListener = errorResponseListener;

    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Activity getContext() {
        return context;
    }

    public void setContext(Activity context) {
        this.context = context;
    }

    public ResponseListener getResponseListener() {
        return responseListener;
    }

    public ErrorResponseListener getErrorResponseListener() {
        return errorResponseListener;
    }

    public void setResponseListener(ResponseListener responseListener) {
        this.responseListener = responseListener;
    }

    public void setErrorResponseListener(ErrorResponseListener errorResponseListener) {
        this.errorResponseListener = errorResponseListener;
    }

    /**
     * this method creates a background thread to execute the request
     * @param requestURL the url of the request to be executed
     */
    public abstract void load(String requestURL);

}
