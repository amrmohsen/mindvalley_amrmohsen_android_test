package mindvalley.com.universalloaderlib.request.datatype;

import android.app.Activity;
import android.content.ComponentCallbacks2;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import mindvalley.com.universalloaderlib.util.MemoryCache;

/**
 * This class creates a custom AsyncTaskLoader to get an image as Bitmap
 */
public class ImageLoader extends Loader implements ComponentCallbacks2 {


    public ImageLoader(Activity context, String urlStr) {
        super(context, urlStr);
        getContext().registerComponentCallbacks(this);

    }

    @Override
    public Bitmap loadInBackground() {

        Bitmap bitmap = MemoryCache.imageCache.get(getUrlStr());

        if (bitmap == null) {
            try {

                URL url = new URL(getUrlStr());
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream input = connection.getInputStream();
                bitmap = BitmapFactory.decodeStream(input);

                MemoryCache.imageCache.put(getUrlStr(), bitmap);
            } catch (Exception ex) {
                Log.d("error", ex.toString());
            }
        }

        return bitmap;

    }

    @Override
    public void onTrimMemory(int level) {
        if (level >= TRIM_MEMORY_MODERATE) {
            MemoryCache.imageCache.evictAll();
        } else if (level >= TRIM_MEMORY_BACKGROUND) {
            MemoryCache.imageCache.trimToSize(MemoryCache.imageCache.size() / 2);
        }
    }

    @Override
    public void onConfigurationChanged(Configuration configuration) {

    }

    @Override
    public void onLowMemory() {

    }

}