package mindvalley.com.universalloaderlib.request.datatype;

import android.app.Activity;
import android.app.LoaderManager;
import android.content.Loader;
import android.graphics.Bitmap;
import android.os.Bundle;

import mindvalley.com.universalloaderlib.listeners.ErrorResponseListener;
import mindvalley.com.universalloaderlib.listeners.ResponseListener;
import mindvalley.com.universalloaderlib.request.Request;

/**
 * Class to create Image Request object and use it to get bitmap
 */

public class ImageRequest extends Request {

    private ImageLoader imageLoader;

    public ImageRequest(Activity context) {
        super(context);
    }

    /**
     * @param id                    the unique id to create the request
     * @param context               the activity context
     * @param responseListener      new response listener to get the data when the request is successfully finished
     * @param errorResponseListener new error response listener when the request is failed to be executed
     */
    public ImageRequest(int id, Activity context, ResponseListener responseListener, ErrorResponseListener errorResponseListener) {
        super(id, context, responseListener, errorResponseListener);
    }

    /**
     * this method creates a background thread to execute the request
     *
     * @param requestURL the url of the request to be executed
     */
    @Override
    public void load(final String requestURL) {

        getContext().getLoaderManager().initLoader(getId(), null, new LoaderManager.LoaderCallbacks<Bitmap>() {


            @Override
            public Loader<Bitmap> onCreateLoader(int i, Bundle bundle) {
                imageLoader = new ImageLoader(getContext(), requestURL);
                return (Loader) imageLoader;
            }

            @Override
            public void onLoadFinished(Loader<Bitmap> loader, Bitmap bitmap) {

                try {
                    if (bitmap != null)
                        getResponseListener().onSuccess(bitmap);
                    else
                        getErrorResponseListener().onError(new NullPointerException());
                } catch (Exception e) {
                    getErrorResponseListener().onError(e);
                }
            }

            @Override
            public void onLoaderReset(Loader<Bitmap> loader) {
                if (imageLoader!=null)
                imageLoader.stopLoading();

            }
        });


    }

    /**
     * this method cancels the request if still being executed
     */
    public void cancelLoad() {

        imageLoader.reset();
    }


}
