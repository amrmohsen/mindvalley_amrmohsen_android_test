package mindvalley.com.universalloaderlib.request.datatype;

import android.app.Activity;
import android.app.LoaderManager;
import android.content.Loader;
import android.os.Bundle;

import mindvalley.com.universalloaderlib.listeners.ErrorResponseListener;
import mindvalley.com.universalloaderlib.listeners.ResponseListener;
import mindvalley.com.universalloaderlib.request.Request;

/**
 * Class to create Json Request object and use it to get json file
 */

public class JsonRequest extends Request {


    private JsonLoader jsonLoader;

    public JsonRequest(Activity context) {
        super(context);
    }

    /**
     * @param id                    the unique id to create the request
     * @param context               the activity context
     * @param responseListener      new response listener to get the data when the request is successfully finished
     * @param errorResponseListener new error response listener when the request is failed to be executed
     */
    public JsonRequest(int id, Activity context, ResponseListener responseListener, ErrorResponseListener errorResponseListener) {
        super(id, context, responseListener, errorResponseListener);
    }

    /**
     * this method creates a background thread to execute the request
     * @param requestURL the url of the request to be executed
     */
    @Override
    public void load(final String requestURL) {

        getContext().getLoaderManager().initLoader(getId(), null, new LoaderManager.LoaderCallbacks<String>() {

            @Override
            public Loader<String> onCreateLoader(int i, Bundle bundle) {
                jsonLoader = new JsonLoader(getContext(), requestURL);
                return (Loader) jsonLoader;
            }

            @Override
            public void onLoadFinished(android.content.Loader<String> loader, String jsonStr) {

                try {
                    getResponseListener().onSuccess(jsonStr);
                } catch (Exception e) {
                    getErrorResponseListener().onError(e);
                }
            }

            @Override
            public void onLoaderReset(android.content.Loader<String> loader) {
                if (jsonLoader != null)
                    jsonLoader.stopLoading();
            }
        });

    }

    /**
     * this method cancels the request if still being executed
     */
    public void cancelLoad() {

        jsonLoader.reset();
    }

}
