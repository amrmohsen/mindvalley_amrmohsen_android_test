package mindvalley.com.universalloaderlib.request.datatype;

import android.app.Activity;
import android.content.AsyncTaskLoader;

/**
 * Created by Amr on 8/28/16.
 */

public class Loader extends AsyncTaskLoader<Object>  {

    private String urlStr;


    public Loader(Activity context, String urlStr) {
        super(context);
        this.urlStr = urlStr;
        onContentChanged();
    }

    public String getUrlStr() {
        return urlStr;
    }

    public void setUrlStr(String urlStr) {
        this.urlStr = urlStr;
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        if (takeContentChanged())
            forceLoad();
    }

    @Override
    public Object loadInBackground() {
        return null;
    }


}
