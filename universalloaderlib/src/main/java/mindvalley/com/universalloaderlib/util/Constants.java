package mindvalley.com.universalloaderlib.util;

/**
 * Created by Amr on 8/28/16.
 */

public class Constants {

    // Get max available VM memory, exceeding this amount will throw an
    // OutOfMemory exception. Stored in kilobytes as LruCache takes an
    // int in its constructor.
    public final static int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);

    // Use 1/8th of the available memory for this memory cache.
    public final static int CACHE_SIZE = maxMemory / 8;

}
