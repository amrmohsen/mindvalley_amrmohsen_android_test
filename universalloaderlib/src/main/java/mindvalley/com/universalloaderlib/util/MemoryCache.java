package mindvalley.com.universalloaderlib.util;

import android.graphics.Bitmap;
import android.util.LruCache;

import java.util.HashMap;

/**
 * Class to put data sets to cache data in memory
 */

public class MemoryCache {

    public static HashMap<String,String> jsonCache=new HashMap<>();
    public static LruCache<String,Bitmap> imageCache=new LruCache<>(Constants.CACHE_SIZE);


}
